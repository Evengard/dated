#include <QtGui/QApplication>
#include <QSplashScreen>
#include <QThread>
#include <QTime>

#include "MainWindow.h"
#include "Config.h"

class Sleeper : public QThread
{
public:
    static void sleep(unsigned long secs) { QThread::sleep(secs); }
};

/**
 * ~
 *
 * @author Yorie
 */

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    qsrand((uint)QTime::currentTime().msec());
    QPixmap pix((qrand() % 2 == 0) ? ":/splash/splash_0" : ":/splash/splash_1");
    QSplashScreen* splash = new QSplashScreen(pix);
    splash->show();
    splash->showMessage(QString("Version %1").arg(VERSION), Qt::AlignBottom, Qt::white);

    a.processEvents();
    Sleeper::sleep(3);

    MainWindow w;
    w.show();
    splash->finish(&w);

    
    return a.exec();
}
