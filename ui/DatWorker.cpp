#include "DatWorker.h"
#include <QObject>
#include <QMessageBox>

void DatWorker::pack(QString file)
{
    if (process.isOpen())
    {
        process.close();
    }

    QStringList args;
    args.append(file.replace("/", "\\"));
    args.append("--");
    args.append("tmp");
    args.append("--");
    args.append("e");
    process.start("\"" + datedPath + "\"", args);
}

void DatWorker::unpack(QString file)
{
    if (process.isOpen())
    {
        process.close();
    }

    QStringList args;
    args.append(file.replace("/", "\\"));
    args.append("--");
    args.append("tmp");
    args.append("--");
    args.append("d");
    process.start("\"" + datedPath + "\"", args);
    if (process.error() != 5)
    {
        QMessageBox msg;
        msg.setText("Cannot run unpack process! Error code: " + QString("%1").number(process.error()));
        msg.exec();
    }
}
