#ifndef UTIL_H
#define UTIL_H

#include <QList>
#include <QString>

class Util
{
public:
    static QList<QString> listFiles(QString path);
    static void removeAll(QString path);
};

#endif // UTIL_H
