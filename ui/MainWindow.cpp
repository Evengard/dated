#include <QFileDialog>
#include <QSettings>
#include <QFile>
#include <QLabel>
#include <QComboBox>
#include <QGridLayout>
#include <QTextCodec>
#include <QPushButton>

/**
 * Main window impl.
 *
 * @author Yorie
 */

#include "MainWindow.h"
#include "ui_MainWindow.h"
#include "DatWorker.h"
#include "Util.h"
#include "Config.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    fileList(0),
    dataEdit(0),
    logEdit(0),
    editable(0),
    textChanged(false),
    btnSave(0),
    currentFile(""),
    lastDatFileName(""),
    about(0),
    searchBox(0)
{
    ui->setupUi(this);
    init();
}

void MainWindow::init()
{
    setWindowTitle(windowTitle().replace("{%version%}", VERSION));
    setWindowIcon(QIcon(":/main/icon"));

    connect(ui->actionQuit, SIGNAL(triggered()), this, SLOT(close()));
    connect(ui->actionOpen, SIGNAL(triggered()), this, SLOT(openDat()));
    connect(ui->actionSave, SIGNAL(triggered()), this, SLOT(saveDat()));
    connect(ui->actionSearch, SIGNAL(triggered()), this, SLOT(openSearch()));
    connect(ui->actionAbout, SIGNAL(triggered()), this, SLOT(onAboutClicked()));

    connect(DatWorker::getInstance()->getProcess(), SIGNAL(readyReadStandardOutput()), this, SLOT(outputReady()));
    connect(DatWorker::getInstance()->getProcess(), SIGNAL(finished(int)), this, SLOT(workerFinished()));

    // Add versions of DatEd
    proto = new QComboBox(this);
    QLabel* version = new QLabel(tr("Version: "));
    bool isFirstProto = true;
    foreach (QString dir, QDir("dat").entryList(QDir::Dirs | QDir::NoSymLinks | QDir::NoDotAndDotDot))
    {
        if (isFirstProto)
        {
            DatWorker::getInstance()->setDatedPath("dat/" + dir + "/dated.exe");
            isFirstProto = false;
        }

        proto->addItem(dir, dir);
    }
    connect(proto, SIGNAL(currentIndexChanged(int)), this, SLOT(onProtocolVersionChange(int)));
    ui->mainToolBar->addWidget(version);
    ui->mainToolBar->addWidget(proto);

    // Save button
    btnSave = new QPushButton(QIcon(":/main/save"), "Save", this);
    btnSave->setEnabled(false);
    connect(btnSave, SIGNAL(clicked()), this, SLOT(onSaveClicked()));
    ui->mainToolBar->addWidget(btnSave);

    // Word-wrap button
    QPushButton* btnWrap = new QPushButton(QIcon(":/main/wrap"), "Word Wrap", this);
    btnWrap->setCheckable(true);
    btnWrap->setChecked(true);
    connect(btnWrap, SIGNAL(toggled(bool)), this, SLOT(onWrapModeToggled(bool)));
    ui->mainToolBar->addWidget(btnWrap);

    QGridLayout* grid = new QGridLayout(ui->centralWidget);
    fileList = new QListWidget(this);
    fileList->setFixedWidth(250);
    grid->addWidget(fileList, 0, 0);
    dataEdit = new QTextEdit(this);
    grid->addWidget(dataEdit, 0, 1);
    logEdit = new QTextEdit(this);
    logEdit->setFixedHeight(150);
    logEdit->setReadOnly(true);
    grid->addWidget(logEdit, 1, 0, 1, 2);

    connect(fileList, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(onFileListDoubleClick(QModelIndex)));
    connect(dataEdit, SIGNAL(textChanged()), this, SLOT(onDataTextChanged()));
}

void MainWindow::onProtocolVersionChange(int index)
{
    DatWorker::getInstance()->setDatedPath("dat/" + proto->itemText(index) + "/dated.exe");
    reopen(lastDatFileName);
}

void MainWindow::onSaveClicked()
{
    if (editable->isOpen() && textChanged)
    {
        QFile writable(editable->fileName());
        writable.open(QFile::WriteOnly);
        writable.write(dataEdit->toPlainText().toUtf8());
        writable.close();
        textChanged = false;
        btnSave->setEnabled(false);
    }
}

void MainWindow::onWrapModeToggled(bool value)
{
    if (value)
        dataEdit->setWordWrapMode(QTextOption::WordWrap);
    else
        dataEdit->setWordWrapMode(QTextOption::NoWrap);
}

void MainWindow::reopen(QString path)
{
    if (path == "")
        return;

    logEdit->clear();
    dataEdit->clear();
    fileList->clear();
    files.clear();
    btnSave->setEnabled(false);

    if (editable && editable->isOpen())
        editable->close();

    DatWorker::getInstance()->unpack(path);

    ui->actionSave->setEnabled(true);
}

void MainWindow::openDat()
{
    QFileDialog* openDialog = new QFileDialog(this, tr("Open dat"), "./", "Dat Files (*.dat)");
    openDialog->setModal(true);

    // Let's store default open directory
    QSettings settings(QSettings::NativeFormat, QSettings::UserScope, "GODWorld", "BnS_DateEditor", this);
    openDialog->setDirectory(settings.value("open_directory", "./").toString());

    // Opened successfully?
    if (openDialog->exec() > 0)
    {
        QStringList files = openDialog->selectedFiles();

        if (files.size() > 0)
        {
            Util::removeAll("tmp");
            settings.setValue("open_directory", openDialog->directory().absolutePath());

            reopen(files.at(0));
            lastDatFileName = files.at(0);
        }
    }
}

void MainWindow::saveDat()
{
    QFileDialog* saveDialog = new QFileDialog(this, tr("Save dat"), "./", "Dat Files (*.dat)");
    saveDialog->setModal(true);

    // Let's store default open directory
    QSettings settings(QSettings::NativeFormat, QSettings::UserScope, "GODWorld", "BnS_DateEditor", this);
    saveDialog->setDirectory(settings.value("save_directory", "./").toString());

    // Selected?
    if (saveDialog->exec() > 0)
    {
        QStringList files = saveDialog->selectedFiles();

        if (files.size() > 0)
        {
            logEdit->clear();
            DatWorker::getInstance()->pack(files.at(0));
        }
    }
}

void MainWindow::outputReady()
{
    logEdit->append(QString(DatWorker::getInstance()->getProcess()->readAllStandardOutput()));
    // Let's fill file list view
    foreach (QString file, Util::listFiles("tmp/"))
    {
        if (!files.contains(file))
        {
            if (file.endsWith(".xml") || file.endsWith(".x16"))
                fileList->addItem(file);

            files.append(file);
        }
    }
}

void MainWindow::onFileListDoubleClick(QModelIndex modelIndex)
{
    if (editable)
    {
        if (editable->isOpen() && textChanged)
        {
            QFile writable(editable->fileName());
            writable.open(QFile::WriteOnly);
            writable.write(dataEdit->toPlainText().toUtf8());
            writable.close();
            textChanged = false;
            btnSave->setEnabled(false);
        }
        editable->close();
        delete editable;
        editable = 0;
    }

    editable = new QFile("tmp/" + modelIndex.data().toString());
    if (!editable->open(QFile::ReadOnly))
        logEdit->append(tr("Cannot open file.\n"));

    QTextCodec *codec = QTextCodec::codecForName("utf-8");
    QString data = codec->toUnicode(editable->readAll());

    dataEdit->setPlainText(data);

    currentFile = "tmp/" + modelIndex.data().toString();
}

void MainWindow::onDataTextChanged()
{
    if (!textChanged && currentFile != "")
    {
        textChanged = true;
        btnSave->setEnabled(true);
    }
}

void MainWindow::openSearch()
{
    if (!searchBox)
    {
        searchBox = new Search(this);
        searchBox->setFixedSize(searchBox->width(), searchBox->height());
        searchBox->setWindowFlags(Qt::Tool);
    }

    // Set up default search text if user selected some text in edit area
    if (!dataEdit->textCursor().selectedText().isEmpty())
        searchBox->setSearchText(dataEdit->textCursor().selectedText());

    searchBox->show();
    searchBox->activateWindow();
}

QTextEdit* MainWindow::getTextEdit()
{
    return dataEdit;
}

void MainWindow::onAboutClicked()
{
    if (!about)
    {
        about = new About(this);
        about->setVersion(VERSION);
    }

    about->show();
    about->activateWindow();
}

void MainWindow::workerFinished()
{

}

void MainWindow::closeEvent(QCloseEvent *)
{
    // Remove trash
    Util::removeAll("tmp");
}

MainWindow::~MainWindow()
{
    if (searchBox)
    {
        searchBox->close();
        delete searchBox;
    }

    if (about)
    {
        about->close();
        delete about;
    }

    delete ui;
}
